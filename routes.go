package main

import (
	controller "GoCrud/controllers"
	"GoCrud/models"
	"github.com/gin-gonic/gin"
)

func main() {
	// connect with database
	models.ConnectDB()

	router := gin.Default()

	// Simple group: v1
	v1 := router.Group("/v1")
	{
		v1.GET("/agent/list", controller.GetAgent)
	}

	router.Run(":3000")
}
