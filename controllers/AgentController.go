package controllers

import (
	"GoCrud/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetAgent(c *gin.Context) {
	//var agents []models.AgentAgent
	//models.DB.Find(&agents)
	//
	//c.JSON(http.StatusOK, gin.H{
	//	//"data": agents,
	//	"data": agents,
	//})

	var agent_details []models.AgentAgentdetails
	//models.DB.Unscoped().Find(&agent_details)
	models.DB.Unscoped().Preload("agentId").Find(&agent_details)

	c.JSON(http.StatusOK, gin.H{
		//"data": agents,
		"data": agent_details,
	})
}
