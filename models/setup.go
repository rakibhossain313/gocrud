package models

import (
	"fmt"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"os"
)

var DB *gorm.DB

func ConnectDB() {
	env_err := godotenv.Load(".env")
	if env_err != nil {
		log.Fatalf("Error loading .env file")
	}

	db_host := os.Getenv("DB_HOST")
	db_user := os.Getenv("DB_USER")
	db_name := os.Getenv("DB_NAME")
	db_password := os.Getenv("DB_PASSWORD")
	db_port := os.Getenv("DB_PORT")
	db_ssl_mode := os.Getenv("SSL_MODE")
	db_time_zone := os.Getenv("TIME_ZONE")

	// creat connection with database
	dsn := "host=" + db_host + " user=" + db_user + " password=" + db_password + " dbname=" + db_name + " port=" + db_port + " sslmode=" + db_ssl_mode + " TimeZone=" + db_time_zone
	database, db_err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})

	if db_err != nil {
		fmt.Println(db_err)
	} else {
		fmt.Println("| ========================================= |")
		fmt.Println("| Database has been connected successfully  |")
		fmt.Println("| ========================================= |")
	}

	DB = database
}
