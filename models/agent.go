package models

type AgentAgent struct {
	WalletNumber string `json:"wallet_number"`
	FirstName    string `json:"first_name"`
}

type AgentAgentdetails struct {
	//gorm.Model

	agentId        AgentAgent `gorm:"ForeignKey:AgentID"`
	PresentAddress string     `json:"present_address"`
	Nationality    string     `json:"nationality"`
}
